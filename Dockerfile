FROM golang:1.15.5-buster AS build-env
WORKDIR /server
COPY . .
RUN go get
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags '-w -s -extldflags "-static"' -o server
RUN chmod +x server

FROM scratch

WORKDIR /

COPY --from=build-env /server/server /server
COPY assets /assets
COPY views /views

EXPOSE 3000

ENTRYPOINT ["/server"]

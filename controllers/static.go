package controllers

import (
	"lenslocked.com/views"
)

func NewStatic() *Static {
	return &Static{
		Contact: views.NewView("bootstrap", "static/contact"),
	}
}

type Static struct {
	Contact *views.View
}

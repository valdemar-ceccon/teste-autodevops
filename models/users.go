package models

import (
	"golang.org/x/crypto/bcrypt"
	"lenslocked.com/hash"

	"github.com/jinzhu/gorm"
)

//const userPwPepper = "lL7#53SQsqzS@aSTFO&K"
//const hmacSecretKey = "secret-hmac-key"

var _ UserDB = &userGorm{}

// UserDB is used to interact with the users database.
type UserDB interface {
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByRemember(token string) (*User, error)

	Create(user *User) error
	Update(user *User) error
	Delete(id uint) error
	Forget(user *User) error
}

// UserService is a set of methods used to manipulate
// and work with user models
type UserService interface {
	// Authenticate will verify the provided email and password
	// are correct. If they are correct, the user will be returned.
	// Otherwise you will receive either:
	// ErrNotFound, ErrPasswordIncorrect, or another error if something goes
	// wrong
	Authenticate(email, password string) (*User, error)
	UserDB
}

// NewUserService create a new UserService for a given database
func NewUserService(db *gorm.DB, pepper, hmacSecretKey string) UserService {
	ug := &userGorm{db}
	hmac := hash.NewHMAC(hmacSecretKey)
	uv := newUserValidator(ug, hmac, pepper)
	return &userService{
		UserDB: uv,
		userPwPepper: pepper,
	}
}

// User model for a user of the app
type User struct {
	gorm.Model
	Name         string
	Email        string `gorm:"not null;unique_index"`
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	Remember     string `gorm:"-"`
	RememberHash string `gorm:"not null,unique_index"`
}

type userService struct {
	UserDB
	userPwPepper string
}

type userGorm struct {
	db *gorm.DB
}

// ByID will look up the user using the id provided
// 1 - user, nil
// 2 - nil, ErrNotFound
// 3 - nil, otherError
func (ug *userGorm) ByID(id uint) (*User, error) {
	var user User
	db := ug.db.Where("id = ?", id)
	err := first(db, &user)
	return &user, err
}

// ByEmail look up the user by the provided email
func (ug *userGorm) ByEmail(email string) (*User, error) {
	var user User
	db := ug.db.Where("email = ?", email)
	err := first(db, &user)
	return &user, err
}

// ByRemember looks up the user by the remember token and return that user
// This method will handle hashing
func (ug *userGorm) ByRemember(rememberHash string) (*User, error) {
	var user User
	err := first(ug.db.Where("remember_hash = ?", rememberHash), &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func first(db *gorm.DB, dst interface{}) error {
	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}

	return err
}

// Authenticate a user with provided email address and password
func (us *userService) Authenticate(email, password string) (*User, error) {
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(foundUser.PasswordHash), []byte(password+us.userPwPepper))
	if err != nil {
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			return nil, ErrPasswordIncorrect
		default:
			return nil, err
		}
	}
	return foundUser, nil
}

// Create a new user in the database and update the provided pointer
// with the id of the new user
func (ug *userGorm) Create(user *User) error {
	return ug.db.Create(user).Error
}

// Update a provided user with all of the data
// in the provided object
func (ug *userGorm) Update(user *User) error {
	return ug.db.Save(user).Error
}

// Delete the user with the provided id
func (ug *userGorm) Delete(id uint) error {
	user := User{Model: gorm.Model{ID: id}}
	return ug.db.Delete(&user).Error
}

func (ug *userGorm) Forget(user *User) error {
	return ug.db.Model(&User{}).Where("id = ?", user.ID).Update("remember_hash", "").Error
}
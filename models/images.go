package models

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Image struct {
	GalleryID uint
	Filename  string
}

func (i *Image) String() string {
	return i.Path()
}

func (i *Image) Path() string {
	temp := url.URL{
		Path: "/" + i.RelativePath(),
	}

	return temp.String()
}

func (i *Image) RelativePath() string {
	return fmt.Sprintf("images/galleries/%v/%v", i.GalleryID, i.Filename)
}

type ImageService interface {
	Create(galleryID uint, reader io.ReadCloser, filename string) error
	ByGalleryID(galleryID uint) ([]Image, error)
	Delete(image *Image) error
}

func NewImageService(imageRoot string) ImageService {
	return &imageService{imageRoot: imageRoot}
}

type imageService struct {
	imageRoot string
}

func (is *imageService) ByGalleryID(galleryID uint) ([]Image, error) {
	path := is.galleryPath(galleryID)
	imgStrs, err := filepath.Glob(path + "*")
	if err != nil {
		return nil, err
	}
	ret := make([]Image, len(imgStrs))
	for i := range imgStrs {
		imgStrs[i] = strings.Replace(imgStrs[i], path, "", 1)
		ret[i] = Image{
			Filename:  imgStrs[i],
			GalleryID: galleryID,
		}
		imgStrs[i] = "/" + imgStrs[i]
	}
	return ret, nil
}

func (is *imageService) galleryPath(galleryID uint) string {
	return filepath.Join(is.imageRoot, "galleries", strconv.Itoa(int(galleryID))) + string(os.PathSeparator)
}

func (is *imageService) imagePath(image *Image) string {
	return filepath.Join(is.galleryPath(image.GalleryID), image.Filename)
}

func (is *imageService) Create(galleryID uint, r io.ReadCloser, filename string) error {
	defer r.Close()
	path, err := is.makeImagePath(galleryID)
	if err != nil {
		return err
	}

	dstPath := filepath.Join(path, filename)



	dst, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer dst.Close()

	_, err = io.Copy(dst, r)

	_, err = isImage(dstPath)
	if err != nil {
		return err
	}

	if err != nil {
		_ = os.Remove(dstPath)
		return err
	}
	return nil
}

func isImage(img string) (bool, error) {
	file, err := os.Open(img)
	if err != nil {
		return false, err
	}
	defer file.Close()
	inicio := make([]byte, 512)
	_, err = file.Read(inicio)
	if err != nil {
		return false, err
	}
	if !strings.HasPrefix(http.DetectContentType(inicio), "image/") {
		return false, ImageError("invalid image")
	}

	return true, nil
}

type ImageError string

func (i ImageError) Error() string {
	return string(i)
}

func (i ImageError) Public() string {
	return string(i)
}

func (is *imageService) Delete(image *Image) error {
	imagePath := is.imagePath(image)
	return os.Remove(imagePath)
}

func (is *imageService) makeImagePath(galleryID uint) (string, error) {
	galleryPath := is.galleryPath(galleryID)
	err := os.MkdirAll(galleryPath, 0755)
	if err != nil {
		return "", err
	}
	return galleryPath, nil
}

package models

import (
	"github.com/jinzhu/gorm"
	// data base sql driver
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ServicesConfig func(*Services) error
type ServicesDBConfig func(*Services) error

func WithGorm(dialect, connectionInfo string) ServicesDBConfig {
	return func(services *Services) error {
		db, err := gorm.Open(dialect, connectionInfo)
		if err != nil {
			return err
		}
		services.db = db
		return nil
	}
}

func WithLogMode(mode bool) ServicesConfig {
	return func(services *Services) error {
		services.db.LogMode(mode)
		return nil
	}
}

func WithUser(pepper, hmacKey string) ServicesConfig {
	return func(services *Services) error {
		services.User = NewUserService(services.db, pepper, hmacKey)
		return nil
	}
}

func WithGallery() ServicesConfig {
	return func(services *Services) error {
		services.Gallery = NewGalleryService(services.db)
		return nil
	}
}

func WithImage(imageRoot string) ServicesConfig {
	return func(services *Services) error {
		services.Image = NewImageService(imageRoot)
		return nil
	}
}

// NewServices take connectionInfo and create all app services
func NewServices(dbconfig ServicesDBConfig, cfgs ... ServicesConfig) (*Services, error) {
	var s Services
	var err error

	if err = dbconfig(&s); err != nil {
		return nil, err
	}

	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}

	return &s, nil
}

// Services group all apps services, so they can share
// commom resources
type Services struct {
	Gallery GalleryService
	User    UserService
	Image   ImageService
	db      *gorm.DB
}

// Close underlying database connection
func (s *Services) Close() error {
	return s.db.Close()
}

// DestructiveReset drops all tables and rebuilds them
func (s *Services) DestructiveReset() error {
	err := s.db.DropTableIfExists(
		&User{},
		&Gallery{}).Error
	if err != nil {
		return err
	}
	return s.AutoMigrate()
}

// AutoMigrate will atempt to automatically migrate all tables
func (s *Services) AutoMigrate() error {
	return s.db.AutoMigrate(
		&User{},
		&Gallery{}).Error
}

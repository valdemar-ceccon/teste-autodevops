module lenslocked.com

go 1.15

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gorilla/csrf v1.7.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
